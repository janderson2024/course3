# Hey

## Using this code in colab

```python
#RUN ONCE
import httpimport
with httpimport.gitlab_repo('janderson2024', 'course3', ref="main"):
    import structures
```

## Example using the SingleLinkedList

```python
test_list = structures.SingleLinkedList()
test_list.add_data("a")
test_list.add_data("b")
test_list.add_data("c")
test_list.add_data("d")
test_list.add_data("e")
test_list.add_data("f")

print(test_list)
print("-------")
for item in test_list:
    print(item)

test_list.del_head()
print("-------")
print(test_list)
print("-------")
print(test_list.get_predecessor("d"))
```
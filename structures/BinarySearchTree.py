class BinarySearchNode:

    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def insert(self, data):
        if data < self.data:
            if not self.left:
                self.left = BinarySearchNode(data)
            else:
                self.left.insert(data)
        elif data > self.data:
            if not self.right:
                self.right = BinarySearchNode(data)
            else:
                self.right.insert(data)

    def search(self, value):
        if value < self.data:
            if not self.left:
                return False
            else:
                return self.left.search(value)
        elif value > self.data:
            if not self.right:
                return False
            else:
                return self.right.search(value)
        return True
    
    def __repr__(self):
        return f'data: {self.data}, left:[{self.left.data if self.left else None}], right:[{self.right.data if self.right else None}]'

class BinarySearchTree:

    def __init__(self, root=None):
        self.root = root

    def __repr__(self):
        return f'root: {self.root}, left:||{self.root.left}||, right:||{self.root.right}||'

    def add_data(self, data):
        if not self.root:
            self.root = BinarySearchNode(data)
            return
        
        self.root.insert(data)
    
    def search(self, value):
        if not self.root:
            return False
        return self.root.search(value)
    
    def inorder_traversal(self, node=None, root=True):
        if root:
            node = self.root
        if not node:
            return []

        return self.inorder_traversal(node.left, root=False) + [node.data] + self.inorder_traversal(node.right, root=False)

    def preorder_traversal(self, node=None, root=True):
        if root:
            node = self.root
        if not node:
            return []

        return [node.data] + self.preorder_traversal(node.left, root=False) + self.preorder_traversal(node.right, root=False)

    def postorder_traversal(self, node=None, root=True):
        if root:
            node = self.root
        if not node:
            return []

        return self.postorder_traversal(node.left, root=False) + self.postorder_traversal(node.right, root=False) + [node.data]
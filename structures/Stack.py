class StackNode:
    def __init__(self, data, next = None):
        self.data = data
        self.next = next

    def __repr__(self):
        return f'data: {self.data}, next:[{self.next.data if self.next else None}]'


class Stack:
    def __init__(self):
        self.head = None

    def __len__(self):
        count = 0
        for _ in self:
            count += 1
        return count

    def __contains__(self, value):
        for val in self:
            if val is value:
                return True
        return False

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current:
            node = self.current
            self.current = node.next
            return node
        else:
            raise StopIteration

    def __repr__(self):
        return "Stack: " + ", ".join([f'Node[{node}]' for node in self])
    
    def push(self, data):
        new_node = StackNode(data, self.head)
        self.head = new_node

    def pop(self):
        node = self.head
        self.head = node.next
        return node.data

    
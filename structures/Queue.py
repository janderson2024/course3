class QueueNode:
    def __init__(self, data, next = None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev

    def __repr__(self):
        return f'data: {self.data}, next:[{self.next.data if self.next else None}], prev:[{self.prev.data if self.prev else None}]'

class Queue:
    def __init__(self):
        self.head = None
        self.tail = None

    def __len__(self):
        count = 0
        for _ in self:
            count += 1
        return count

    def __contains__(self, value):
        for val in self:
            if val is value:
                return True
        return False

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current:
            node = self.current
            self.current = node.next
            return node
        else:
            raise StopIteration

    def __repr__(self):
        return "Queue: " + ", ".join([f'Node[{node}]' for node in self])

    def push(self, data):
        new_node = QueueNode(data, self.head)

        if self.head:
            self.head.prev = new_node

        if not self.tail:
            self.tail = new_node

        self.head = new_node

    def pop(self):
        node = self.tail
        self.tail = node.prev

        if self.tail:
            self.tail.next = None
        else:
          self.head = None  

        return node.data
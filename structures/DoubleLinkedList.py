## Used for the DoubleLinkedList class
class DoubleLinkedNode:
    def __init__(self, data, next=None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev

    def __repr__(self):
        return f'data: {self.data}, next:[{self.next.data if self.next else None}], prev:[{self.prev.data if self.prev else None}]'


class DoubleLinkedList:
    def __init__(self):
        self.head = None
        self.tail = self.head

    def add_data_to_head(self, data):
        new_node = DoubleLinkedNode(data, self.head)
        if self.head:
            self.head.prev = new_node
        self.head = new_node
        if not self.tail:
            self.tail = new_node
    
    def add_data_to_tail(self, data):
        new_node = DoubleLinkedNode(data, None, self.tail)
        if self.tail:
            self.tail.next = new_node
        self.tail = new_node
        if not self.head:
            self.head = new_node

    def del_head(self):
        self.head = self.head.next

    def del_tail(self):
        self.tail = self.tail.prev

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current:
            node = self.current
            self.current = node.next
            return node
        else:
            raise StopIteration

    def __repr__(self):
        return "List: " + ", ".join([f'Node[{node}]' for node in self])

    def __contains__(self, value):
        for val in self:
            if val is value:
                return True
        return False
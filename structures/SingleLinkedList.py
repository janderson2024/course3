## Used for the SingleLinkedList class
class SingleLinkedNode:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next

    def __repr__(self):
        return f'data: {self.data}, next:[{self.next.data if self.next else None}]'

class SingleLinkedList:
    ## General Object stuff
    def __init__(self):
        self.head = None

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current:
            node = self.current
            self.current = node.next
            return node
        else:
            raise StopIteration

    def __repr__(self):
        return "List: " + ", ".join([f'Node[{node}]' for node in self])

    def __len__(self):
        count = 0
        for _ in self:
            count += 1
        return count

    def __contains__(self, value):
        for val in self:
            if val is value:
                return True
        return False

    ## Custom Methods

    def get_head(self):
        return self.head
    
    def is_empty(self):
        return self.head is None 

    def has_loop(self):
        start = self.get_head()

        node = start.next
        while node:
            if node is start:
                return True
            node = node.next
        return False

    def insert_at_head(self, data):
        new_node = SingleLinkedNode(data, self.head)
        self.head = new_node

    def insert_at_tail(self, data):
        new_node = SingleLinkedNode(data)

        if not self.head:
            self.head = new_node
            return

        for node in self:
            pass
        node.next = new_node
        return

    # adds after the node with the given value, or throws
    def insert_after(self, value, new_data):
        new_node = SingleLinkedNode(new_data)

        for node in self:
            if node.data is value:
                new_node.next = node.next
                node.next = new_node
                return
        raise Exception("No such value in linked list")

    # add at the provided position or throws
    def insert_at(self, position, data):
        if position < 0 or position > len(self):
            raise Exception("Position out of bounds")
        new_node = SingleLinkedNode(data)
        count = 1
        for node in self:
            if count is position:
                new_node.next = node.next
                node.next = new_node
                return
            count += 1
        node.next = new_node


    #removes the first node from the list
    def del_head(self):
        if(self.head):
            self.head = self.head.next

    def del_all(self):
        self.head = None

    def del_value(self, value):
        prev_node = None
        if self.get_head().data is value:
            self.del_head()
            return True

        for node in self:
            if node.data is value:
                if prev_node:
                    prev_node.next = node.next
                return True
            prev_node = node
        return False
    
        
    def reverse(self):
        prev = None
        next = None
        curr = self.get_head()

        while curr:
            next = curr.next
            curr.next = prev
            prev = curr
            curr = next
            
        self.head = prev
    
    #returns the predecssor to the node with the provided data
    def get_predecessor(self, data):
        prev_node = None

        for node in self:
            if node.data == data:
                return prev_node
            prev_node = node

        return None
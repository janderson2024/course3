from .SingleLinkedList import *
from .DoubleLinkedList import *
from .Stack import *
from .Queue import *


def test():
	print("Structure package is properly loaded!")
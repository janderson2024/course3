import sys
sys.path.insert(0, "..")
from structures import SingleLinkedList

test_ll = SingleLinkedList()
print(test_ll.is_empty())
test_ll.insert_at_head(3)
test_ll.insert_at_head(2)
test_ll.insert_at_head(1)
test_ll.insert_at_tail(4)
print(test_ll.is_empty())
print(test_ll)
print(len(test_ll))

for node in test_ll:
    print(node)


print(3 in test_ll)
print(7 in test_ll)

test_ll.insert_at(2,7)
print(test_ll)

test_ll.del_value(3)

print(test_ll)
test_ll.reverse()
print(test_ll)

try:
    test_ll.insert_at(21, 8)
except Exception:
    print()